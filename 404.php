<?php get_header(); ?>

<section class="hero">
	<div class="container">
		<div class="cols">
			<div class="col is-12">
				<h1 class="load-hidden">404</h1>
				<p class="load-hidden">It looks like this page doesn't exist! <a href="/">Back to home</a>.</p>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
