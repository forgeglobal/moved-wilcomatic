document.addEventListener("DOMContentLoaded", () => {

  const sr = ScrollReveal({ distance: '60px', duration: 1000, delay: 200 });
  const oldschool = '-ms-scroll-limit' in document.documentElement.style && '-ms-ime-align' in document.documentElement.style;
  const unsupported = (function() { if (new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})").exec(navigator.userAgent) != null) { return parseFloat( RegExp.$1 ); } else { return false; } })();

  if (oldschool) {
    document.querySelector('html').classList.add('oldschool');
  }

  setTimeout(() => {
    document.querySelector('.header__logo').classList.add('loaded');
  }, 400);

  sr.reveal('.load-hidden');

  if (window.innerWidth > 1024) {
    if (document.querySelector('.rellax')) {
      var rellax = new Rellax('.rellax', {
        center: true,
      });
    }
  }

  /**
   * .forEach() polyfill
   */
  if ('NodeList' in window && !NodeList.prototype.forEach) {
    console.info('polyfill for IE11');
    NodeList.prototype.forEach = function (callback, thisArg) {
      thisArg = thisArg || window;
      for (var i = 0; i < this.length; i++) {
        callback.call(thisArg, this[i], i, this);
      }
    };
  }

  /**
   * callToActionMonitor
   *
   * Tracks scroll distance to display 'call to action' popup when user is 50% down the page
   */
  function callToActionMonitor() {
    if (document.querySelector('.js-cta-popup')) {
      let cta = document.querySelector('.js-cta-popup');
      let pageHeight = document.body.offsetHeight - window.innerHeight;
      let scrollDistance = window.pageYOffset;

      if (scrollDistance > (pageHeight / 2)) {
        cta.classList.add('show');
      } else {
        cta.classList.remove('show');
      }
    }
  }

  /**
   * isInViewport.
   *
   * Detects if element is in the users viewport based on viewport size + scroll position.
   *
   * @param {elem} el - element you want to know if is inside the viewport
   *
   * @return {boolean}
   */
  function isInViewport (el) {
    let top = el.offsetTop;
    let left = el.offsetLeft;
    let width = el.offsetWidth;
    let height = el.offsetHeight;

    while(el.offsetParent) {
      el = el.offsetParent;
      top += el.offsetTop;
      left += el.offsetLeft;
    }

    return (
      top < (window.pageYOffset + window.innerHeight) &&
      left < (window.pageXOffset + window.innerWidth) &&
      (top + height) > window.pageYOffset &&
      (left + width) > window.pageXOffset
    );
  }

  /**
   * isInViewportMonitor.
   *
   * Function to loop through all monitored class names to add class when in view
   */
  function isInViewportMonitor () {
    const elements = [
      '.hero-home',
      '.hero',
      '.hero-contact',
      '.hero-about',
      '.tile',
      '.content-and-statistics__statistic-container__value',
      '.landing__artwork',
    ];

    elements.forEach(function (element, index) {

      let instances = document.querySelectorAll(element);

      instances.forEach(function(instance, index) {

        let trigger = instance;

        // If IE11 / old browser, simply add 'activated' class and don't track scrolling
        if (oldschool) {
          instance.classList.add('activated');
        }
        else {
          // Custom detection to trigger tile animation when paragraph comes into view, not top of tile
          if (element === '.tile') {
            trigger = instance.querySelector('p');
          }

          if (isInViewport(trigger)) {
            instance.classList.add('activated');
          } else {
            // instance.classList.remove('activated');
          }
        }
      });
    });
  }

  /**
   * toggleSubMenu.
   *
   * Calculate total height of submenu items and apply to submenu max-height
   */
  function toggleSubMenu() {
    const submenu = this.querySelector('.sub-menu');
    const children = submenu.children;
    let totalHeight = 0;

    if (!submenu.classList.contains('open')) {
        for (let i = 0; i < children.length; i++) {
            let child = children[i];

            let height = child.offsetHeight;
            let style = getComputedStyle(child);
            totalHeight += (height + parseInt(style.marginTop, 10) + parseInt(style.marginBottom, 10));
        }
        submenu.classList.add('open');
    } else {
        submenu.classList.remove('open');
    }

    submenu.style.maxHeight = `${totalHeight}px`;
  }

  /**
   * toggleMobileMenu.
   *
   * Toggle class names used for mobile menu styles
   */
  function toggleMobileMenu () {
    document.querySelector('html').classList.toggle('mobile-menu-open');
    document.querySelector('.hamburger').classList.toggle('is-active');
    document.querySelector('.header__toggle').classList.toggle('is-active');
    document.querySelector('.header').classList.toggle('is-active');
    document.querySelector('nav').classList.toggle('is-active');
  }


  /**
   * initiateOdometer.
   *
   * Update js-odometer-value using innerHTML to apply odometer effect if element is in view
   *
   * @param {elem} el - element you want to add the odometer effect to
   */
  function initiateOdometer(el) {
    if (isInViewport(el)) {

      if (!el.classList.contains('activated')) {
        el.classList.add('activated');
        let value = el.querySelector('.js-odometer-value').getAttribute('data-value');
        el.querySelector('.js-odometer-value').innerHTML = value;
      }
    }
  }

  function applyHoverOdometer() {
    let valueHolder = this.querySelector('.js-odometer-value');
    let value = valueHolder.getAttribute('data-value');
    valueHolder.innerHTML = value;
  }

  function resetHoverOdometer() {
    let valueHolder = this.querySelector('.js-odometer-value');
    valueHolder.innerHTML = 0;
  }

  // Loop through odometer classes + apply scroll-triggered animation
  if (document.querySelector('.js-odometer-trigger')) {
    let statistics = document.querySelectorAll('.js-odometer-trigger');


    statistics.forEach(function(statistic, index) {
      statistic.querySelector('.js-odometer-value').textContent = '0';

      let args = {
        el: statistic.querySelector('.js-odometer-value'),
        value: 0,
      }

      if (statistic.querySelector('.js-odometer-value.no-comma')) args.format = 'ddddd';

      let od = new Odometer(args);

      document.addEventListener('scroll', () => {
        initiateOdometer(statistic);
      });
      initiateOdometer(statistic);
    });
  }

  // If mobile toggle element exists, run toggleMobileMenu() when clicked
  if (document.querySelector('#mobile-toggle')) {
      document.getElementById('mobile-toggle').addEventListener('click', toggleMobileMenu, false);
  }

  // If submenu exists, run toggleSubMenu() when submenu parent item is clicked
  if (document.querySelector('.menu-item-has-children')) {
      document.querySelector('.menu-item-has-children').addEventListener('click', toggleSubMenu, false);
  }

  // If .js-hover-activated-odometer class exists, run odometer function when mousenter occurs
  if (document.querySelector('.js-odometer-hover-trigger')) {
    let event = 'mouseenter';
    let instances = document.querySelectorAll('.js-odometer-hover-trigger');

    instances.forEach(function(instance, index) {

      instance.querySelector('.js-odometer-value').textContent = '0';

      let od = new Odometer({
        el: instance.querySelector('.js-odometer-value'),
        value: 0,
      });

      instance.addEventListener('mouseenter', applyHoverOdometer, false);
      instance.addEventListener('mouseleave', resetHoverOdometer, false);
    });
  }

  /**
   * scrollEvents.
   *
   * Collection of functions + process to run when user scrolls
   */
  function scrollEvents() {

    // Only run on modern browsers
    if (!oldschool) {
      isInViewportMonitor();
    }

    callToActionMonitor();
  }
  scrollEvents();

  document.addEventListener('scroll', () => {
    scrollEvents();
  });
});
