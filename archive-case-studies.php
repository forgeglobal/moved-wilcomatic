<?php get_header(); ?>

<?php $bg_img = get_field('csa_background_image', 'options'); ?>

<section class="hero theme-<?php the_field('csa_theme', 'options'); ?>" <?php echo ($bg_img) ? 'style="background-image: url('.$bg_img['url'].');"' : ''; ?>>
	<div class="container">
		<div class="cols">
			<div class="col is-12">
				<?php if (get_field('csa_add_pretitle', 'options')) : ?>
					<span class="load-hidden"><?php the_field('csa_pretitle', 'options'); ?></span>
				<?php endif; ?>
				<h1 class="load-hidden"><?php the_field('csa_title', 'options'); ?></h1>
			</div>
		</div>
	</div>
</section>


<?php if (have_posts()) : ?>
	<section class="tiles-two">
		<?php while (have_posts()) : the_post(); ?>

			<?php get_template_part('flexible-blocks/tiles/tile-case-study'); ?>

		<?php endwhile; ?>
		<?php numeric_posts_nav(); ?>
	</section>
<?php endif; ?>

<?php $style = get_field('csa_logos_block_style', 'options'); ?>

<section class="logos <?php echo ($style == 'detailed') ? 'logos--detailed' : ''; ?>">
	<div class="container">
		<div class="cols">
			<?php if (get_field('csa_logos_content', 'options')) : ?>
				<div class="col is-12 load-hidden">
					<?php the_field('csa_logos_content', 'options'); ?>
				</div>
			<?php endif; ?>
			<?php if (have_rows('csa_logos', 'options')) : ?>
					<div class="col is-12 logos__logo-container">
						<?php while (have_rows('csa_logos', 'options')) : the_row(); ?>

								<?php $img = get_sub_field('image'); ?>

								<?php if ($style == 'static') : ?>

										<?php // SIMPLE MARKUP ?>
										<?php if (get_sub_field('url')) : ?>
											<a class="load-hidden" href="<?php the_sub_field('url'); ?>">
												<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" title="<?php echo $img['title']; ?>" />
											</a>
										<?php else : ?>
											<img class="load-hidden" src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" title="<?php echo $img['title']; ?>" />						
										<?php endif; ?>
										<?php // END SIMPLE MARKUP ?>

								<?php else : ?>

										<?php // DETAILED MARKUP ?>

											<div class="detailed-logo-wrapper js-odometer-hover-trigger load-hidden">
												<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" title="<?php echo $img['title']; ?>" />																		
												<div class="detailed-logo-wrapper__content">
													<span class="js-odometer-value" data-value="<?php the_sub_field('stat'); ?>"><?php the_sub_field('stat'); ?></span>
													<?php the_sub_field('description'); ?>
													<?php if ((get_sub_field('url')) && (get_sub_field('link_text'))) : ?>
														<a href="<?php the_sub_field('url'); ?>"><?php the_sub_field('link_text'); ?></a>
													<?php endif; ?>
												</div>
											</div>

										<?php // END DETAILED MARKUP ?>

								<?php endif; ?>
								
							<?php endwhile; ?>
					</div>
			<?php endif; ?>
		</div>
	</div>
</section>


<?php get_footer(); ?>