<?php
// Template name: About template
get_header(); ?>


<?php $bg_img = get_field('background_image'); ?>

<section class="hero-about" <?php echo ($bg_img) ? 'style="background-image: url('.$bg_img['url'].');"' : ''; ?>>
	<div class="hero-about__strip"></div>
	<div class="container">
		<div class="cols">
			<div class="col is-12">
				<div class="hero-about__content load-hidden">
					<?php if (get_field('heading')) : ?>
						<h1><?php the_field('heading'); ?></h1>
					<?php endif; ?>
					<?php if (get_field('leadin_text')) : ?>
						<?php the_field('leadin_text'); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php if (have_rows('statistics')) : ?>
		<div class="container">
			<div class="cols">
				<div class="col is-12">
					<div class="cols">
						<?php while (have_rows('statistics')) : the_row(); ?>
							<div class="col is-12 is-4-sm js-odometer-trigger">
								<div class="hero-about__stat">
									<div class="hero-about__stat__value load-hidden">
										<?php if (get_sub_field('prepend')) : ?>
											<span class="js-prepend"><?php the_sub_field('prepend'); ?></span>
										<?php endif; ?>
										<span class="js-odometer-value <?php echo (get_sub_field('no_comma')) ? 'no-comma' : ''; ?>" data-value="<?php the_sub_field('statistic'); ?>"><?php the_sub_field('statistic'); ?></span>
										<?php if (get_sub_field('append')) : ?>
											<span class="js-append"><?php the_sub_field('append'); ?></span>
										<?php endif; ?>
									</div>
									<?php if (get_sub_field('description')) : ?>
										<p class="hero-about__stat__description load-hidden"><?php the_sub_field('description'); ?></p>
									<?php endif; ?>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>


<?php get_template_part('flexible-blocks/controller'); ?>

<?php get_footer(); ?>
