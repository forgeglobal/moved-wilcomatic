</div>

	<?php if (get_field('show_popup', 'options')) : ?>
		<a href="<?php the_field('popup_link_url', 'options'); ?>" class="helper-popup js-cta-popup">
			<div class="position-relative">
				<h3><?php the_field('popup_title', 'options'); ?></h3>
				<?php the_field('popup_content', 'options'); ?>
				<div class="helper-popup__link-text"><?php the_field('popup_link_text', 'options'); ?></div>
			</div>
		</a>
	<?php endif; ?>

	<footer class="footer <?php echo (get_field('show_popup', 'options')) ? 'footer--extra-padding' : ''; ?>">
		<div class="container">
			<div class="cols is-mobile">
				<div class="col is-12 is-4-md is-3-lg">
					<a class="footer__logo" href="/">
						<?php
						$logo = get_field('footer_logo', 'options');
						if ($logo) : ?>
							<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" title="<?php echo $logo['title']; ?>" />
						<?php endif; ?>
					</a>
					<p>
						&copy; Wilcomatic <?php echo date('Y'); ?>
						<?php if (get_field('privacy_policy_page', 'options')) : ?>
							<br>
							<a href="<?php the_field('privacy_policy_page', 'options'); ?>" target="_blank">Privacy policy</a>
						<?php endif; ?>
							<br>
							<span class="website-by">Website by <a href="https://forge.uk" target="_blank">Forge</a></span>
					</p>
				</div>
				<div class="col is-12 is-3-md is-3-lg no-flex">
					<img class="footer-image" src="<?php echo get_template_directory_uri(); ?>/dist/assets/cws2.jpg" />
				</div>
				<div class="col is-12 is-5-md footer__menu-container">
					<?php wp_nav_menu(array(
						'theme_location' => 'secondary',
						'container' => '',
					)); ?>
					<?php wp_nav_menu(array(
						'theme_location' => 'tertiary',
						'container' => '',
					)); ?>
				</div>
			</div>
		</div>
	</footer>

	<?php wp_footer(); ?>

	</body>
</html>
