<?php
// Template name: Contact template
get_header(); ?>

<section class="hero-contact theme-blue-dark">
	<div class="container">
		<div class="cols">
			<div class="col is-12">
				<?php if (get_field('title')) : ?>
					<h1><?php the_field('title'); ?></h1>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<section class="contact-content">
	<div class="container">
		<div class="cols">
			<div class="col is-12 is-6-md is-3-lg contact-content__content">
				<?php the_field('content'); ?>
			</div>
			<div class="col is-12 is-6-lg contact-content__form-embed">
				<?php the_field('form_embed'); ?>
			</div>
			<div class="col is-12 is-6-md is-3-lg contact-content__contact-details">
				<?php if (get_field('telephone_number', 'options')) : ?>
					<a class="contact contact--telephone" href="tel:<?php the_field('telephone_number', 'options'); ?>"><?php the_field('telephone_number', 'options'); ?></a>
				<?php endif; ?>
				<?php if (get_field('email_address', 'options')) : ?>
					<a class="contact contact--email" href="mailto:<?php the_field('email_address', 'options'); ?>"><?php the_field('email_address', 'options'); ?></a>
				<?php endif; ?>
				<?php if (get_field('address', 'options')) : ?>
					<a class="contact contact--address" href="/"><?php the_field('address', 'options'); ?></a>
				<?php endif; ?>
				<?php if (get_field('google_map_url', 'options')) : ?>
					<a class="google-map-link" target="_blank" href="<?php the_field('google_map_url', 'options'); ?>">View maps</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
