<?php
// Template name: Landing template
get_header(); ?>

<section class="landing theme-<?php the_field('theme'); ?>">
	<div class="landing__artwork theme-<?php the_field('theme'); ?>">
		<?php if (has_post_thumbnail()) : ?>
			<img class="load-hidden" src="<?php the_post_thumbnail_url(); ?>" alt="Download Cover" title="Download Cover" />
		<?php endif; ?>
	</div>
	<div class="landing__content load-hidden">
		<?php if (get_field('title')) : ?>
			<h1><?php the_field('title'); ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; endif; ?>
		<?php if (get_field('form_embed')) : ?>
			<?php the_field('form_embed'); ?>
		<?php endif; ?>
	</div>
</section>

<?php get_footer(); ?>