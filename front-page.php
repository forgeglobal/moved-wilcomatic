<?php // Home page template
get_header(); ?>

<?php $bg_img = get_field('background_image'); ?>

<section class="hero-home" <?php echo ($bg_img) ? 'style="background-image: url('.$bg_img['url'].');"' : ''; ?>>
	<div class="hero-home__strip"></div>
	<div class="container">
		<div class="cols">
			<div class="col is-12">
				<div class="hero-home__content load-hidden">
					<?php if (get_field('heading')) : ?>
						<h1><?php the_field('heading'); ?></h1>
					<?php endif; ?>
					<?php if (get_field('leadin_text')) : ?>
						<?php the_field('leadin_text'); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php if (have_rows('statistics')) : ?>
		<div class="container">
			<div class="cols">
				<div class="col is-12">
					<div class="cols">
						<?php while (have_rows('statistics')) : the_row(); ?>
							<div class="col is-12 is-4-sm js-odometer-trigger">
								<div class="hero-home__stat">
									<div class="hero-home__stat__value load-hidden">
										<?php if (get_sub_field('prepend')) : ?>
											<span class="js-prepend"><?php the_sub_field('prepend'); ?></span>
										<?php endif; ?>
										<span class="js-odometer-value" data-value="<?php the_sub_field('statistic'); ?>"><?php the_sub_field('statistic'); ?></span>
										<?php if (get_sub_field('append')) : ?>
											<span class="js-append"><?php the_sub_field('append'); ?></span>
										<?php endif; ?>
									</div>
									<?php if (get_sub_field('description')) : ?>
										<p class="hero-home__stat__description load-hidden"><?php the_sub_field('description'); ?></p>
									<?php endif; ?>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>

<?php get_template_part('flexible-blocks/controller'); ?>

<?php get_footer(); ?>
