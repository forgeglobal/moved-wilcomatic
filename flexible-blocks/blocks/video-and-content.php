<section class="video-and-content">
	<div class="video-and-content__container  <?php echo (get_sub_field('alignment') == 'video-first') ? 'video-and-content__container--video-first' : ''; ?>">
		<div class="video-and-content__content-container load-hidden">
			<?php if (get_sub_field('include_background_number')) : ?>
				<div class="video-and-content__content-container__bg-text <?php the_sub_field('background_colour'); ?>">
					<?php the_sub_field('content_background'); ?>
				</div>
			<?php endif; ?>
			<?php if (get_sub_field('title')) : ?>
				<h2 class="full-width">
					<?php the_sub_field('title'); ?>
				</h2>
			<?php endif; ?>
			<?php the_sub_field('content'); ?>
		</div>
		<div class="video-and-content__video-container load-hidden">
			<div class="responsive-iframe-container">
				<?php the_sub_field('video_embed'); ?>
			</div>
		</div>
	</div>
</section>