<section class="tiles-two">
	<?php
	
		if (have_rows('tiles')) : while (have_rows('tiles')) : the_row();

			switch (get_sub_field('tile_type')) :

				case 'default' :
					get_template_part('flexible-blocks/tiles/tile');
					break;

				case 'download' :
					get_template_part('flexible-blocks/tiles/tile-download');
					break;

				case 'case-study' :

					$posts = get_sub_field('case_study');

					if ($posts) :
						foreach ($posts as $post) : setup_postdata($post);
							get_template_part('flexible-blocks/tiles/tile-case-study');
						endforeach; wp_reset_postdata();
					endif;
					
					break;

				default :
					get_template_part('flexible-blocks/tiles/tile');

			endswitch;

		endwhile; endif;

	?>
</section>