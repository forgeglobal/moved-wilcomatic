<section class="image-two-columns">
	<div class="image-two-columns__container <?php echo (get_sub_field('alignment') == 'content-first') ? 'content-first' : ''; ?>">
		<?php $img = get_sub_field('image'); ?>
		<div class="image-two-columns__image <?php echo (get_sub_field('trim_block_height')) ? 'image-two-columns__image--trimmed' : ''; ?> <?php echo (get_sub_field('add_padding')) ? 'image-two-columns__image--padded' : ''; ?>">
			<?php if ($img) : ?>
				<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" title="<?php echo $img['title']; ?>" />
			<?php endif; ?>
		</div>
		<div class="image-two-columns__content-container image-two-columns__content-container--<?php the_sub_field('block_background_colour'); ?>">
			<?php if (get_sub_field('include_background_numbers') && (get_sub_field('content_background'))) : ?>
				<div class="image-two-columns__content-container__background <?php the_sub_field('background_colour'); ?>">
					<?php the_sub_field('content_background'); ?>
				</div>
			<?php endif; ?>
			<?php the_sub_field('content'); ?>
		</div>
	</div>
</section>