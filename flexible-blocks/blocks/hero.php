<?php $bg_img = get_sub_field('background_image'); ?>

<section class="hero theme-<?php the_sub_field('theme'); ?>" <?php echo ($bg_img) ? 'style="background-image: url('.$bg_img['url'].');"' : ''; ?>>
	<div class="container">
		<div class="cols">
			<div class="col is-12">
				<?php if (get_sub_field('add_pretitle')) : ?>
					<span class="load-hidden"><?php the_sub_field('pretitle'); ?></span>
				<?php endif; ?>
				<h1 class="load-hidden"><?php the_sub_field('title'); ?></h1>
			</div>
		</div>
	</div>
</section>