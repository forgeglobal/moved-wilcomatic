<section class="content-two-columns">
	<div class="content-two-columns__container <?php echo (get_sub_field('alignment') == 'image-first') ? '' : 'flipped'; ?> <?php echo (get_sub_field('add_pattern')) ? 'content-two-columns__container--pattern' : ''; ?> <?php the_sub_field('theme'); ?>">
		<?php $img = get_sub_field('image'); ?>
		<?php if (get_sub_field('add_logo')) : ?>
			<?php $logo = get_sub_field('logo'); ?>
			<?php if ($logo) : ?>
				<div class="content-two-columns__logo-container load-hidden">
					<img class="content-two-columns__logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" title="<?php echo $logo['title']; ?>" />
				</div>
			<?php endif; ?>
		<?php endif; ?>
 		<div class="content-two-columns__image <?php echo (get_sub_field('trim_block_height')) ? 'content-two-columns__image--trimmed' : ''; ?> <?php echo (get_sub_field('parallax_image')) ? 'rellax' : '' ; ?>" <?php echo (get_sub_field('parallax_image')) ? 'data-rellax-speed="1.5"' : ''; ?> <?php echo ($img) ? 'style="background-image: url('.$img['url'].');"' : ''; ?>></div>
		<div class="content-two-columns__content-container content-two-columns__content-container--<?php the_sub_field('block_background_colour'); ?> <?php echo (get_sub_field('trim_block_height')) ? 'content-two-columns__content-container--trimmed' : ''; ?> <?php echo (get_sub_field('full_width_title')) ? 'has-long-title' : ''; ?>">
			<?php if (get_sub_field('full_width_title')) : ?>
				<h2 class="load-hidden full-width"><?php the_sub_field('full_width_title'); ?></h2>
			<?php endif; ?>
			<div class="content-two-columns__split-container">
				<?php if (get_sub_field('full_width')) : ?>
					<div class="content-two-columns__background-big <?php the_sub_field('background_colour'); ?> load-hidden">
						<?php the_sub_field('content_background'); ?>
					</div>
				<?php endif; ?>
				<?php if (get_sub_field('content')) : ?>
					<div class="content-two-columns__content-container__content content-two-columns__content-container__content--a <?php echo (get_sub_field('split_content')) ? 'content-two-columns__content-container__content--split' : ''; ?> load-hidden">
						<?php if (get_sub_field('include_background_numbers') && !get_sub_field('full_width')) : ?>
							<div class="content-two-columns__content-container__content__background-small <?php the_sub_field('background_colour'); ?>">
								<?php the_sub_field('content_background'); ?>
							</div>
						<?php endif; ?>
						<?php the_sub_field('content'); ?>
					</div>
				<?php endif; ?>
				<?php if (get_sub_field('split_content')) : ?>
					<div class="content-two-columns__content-container__content content-two-columns__content-container__content--b load-hidden">
						<?php if (get_sub_field('content_background_b') && !get_sub_field('full_width')) : ?>
							<div class="content-two-columns__content-container__content__background-small <?php the_sub_field('background_colour'); ?>">
								<?php the_sub_field('content_background_b'); ?>
							</div>
						<?php endif; ?>
						<?php the_sub_field('content_b'); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>