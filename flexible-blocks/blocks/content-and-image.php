<section class="content-and-image <?php the_sub_field('background_colour'); ?>">
	<div class="content-and-image__container <?php echo (get_sub_field('alignment') == 'image-first') ? 'content-and-image__container--flipped' : ''; ?>">
		<div class="content-and-image__container__content load-hidden">
			<?php if (get_sub_field('include_background_number')) : ?>
				<div class="content-and-image__container__content__background">
					<?php the_sub_field('content_background'); ?>
				</div>
			<?php endif; ?>
			<?php the_sub_field('content'); ?>
		</div>
		<?php $img = get_sub_field('image'); ?>
		<div class="content-and-image__container__image <?php echo (get_sub_field('parallax_image')) ? 'rellax' : '' ; ?>" <?php echo (get_sub_field('parallax_image')) ? 'data-rellax-speed="1.5"' : ''; ?> <?php echo ($img) ? 'style="background-image: url('.$img['url'].');"' : ''; ?>></div>
	</div>
</section>