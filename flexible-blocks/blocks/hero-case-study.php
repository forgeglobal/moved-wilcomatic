<section class="hero-case-study">
	<div class="container">
		<div class="cols">
			<div class="col is-12 load-hidden">
				<?php if (get_sub_field('logo')) : $logo = get_sub_field('logo'); ?>
					<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" title="<?php echo $logo['title']; ?>" />
				<?php endif; ?>
				<?php if (get_sub_field('title')) : ?>
					<h1><?php the_sub_field('title'); ?></h1>
				<?php endif; ?>
				<?php if (get_sub_field('leadin_text')) : ?>
					<?php the_sub_field('leadin_text'); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>