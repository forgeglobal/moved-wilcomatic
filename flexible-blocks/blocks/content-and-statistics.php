<section class="content-and-statistics">
	<div class="content-and-statistics__container <?php echo (get_sub_field('alignment') == 'content-first') ? 'content-first' : ''; ?>">
		<div class="content-and-statistics__statistic-container <?php the_sub_field('theme'); ?>">
			<div class="content-and-statistics__statistic-container__value theme-<?php the_sub_field('theme'); ?> js-odometer-trigger load-hidden">
				<?php if (get_sub_field('prepend')) : ?>
					<span class="js-prepend"><?php the_sub_field('prepend'); ?></span>
				<?php endif; ?>
				<span class="js-odometer-value" data-value="<?php the_sub_field('statistic'); ?>"><?php the_sub_field('statistic'); ?></span>
				<?php if (get_sub_field('append')) : ?>
					<span class="js-append"><?php the_sub_field('append'); ?></span>
				<?php endif; ?>
				<?php if (get_sub_field('description')) : ?>
					<?php the_sub_field('description'); ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="content-and-statistics__content-container load-hidden">
			<?php if (get_sub_field('content')) : ?>
				<?php the_sub_field('content'); ?>
			<?php endif; ?>
		</div>
	</div>
</section>