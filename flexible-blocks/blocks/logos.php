<?php $style = get_sub_field('block_style'); ?>

<section class="logos <?php echo ($style == 'detailed') ? 'logos--detailed' : ''; ?> <?php echo (get_sub_field('before_content_logo')) ? 'logos--has-logo' : ''; ?>">
	<div class="container">
		<div class="cols">
			<?php if (get_sub_field('content')) : ?>
				<div class="col is-12 load-hidden <?php the_sub_field('content_style'); ?> max-width-<?php the_sub_field('content_max_width'); ?>">
					<?php if (get_sub_field('before_content_logo')) : ?>
						<?php $before_logo = get_sub_field('before_content_logo'); ?>
						<img class="before-logo" src="<?php echo $before_logo['url']; ?>" alt="<?php echo $before_logo['alt']; ?>" title="<?php echo $before_logo['title']; ?>" />
					<?php endif; ?>
					<?php the_sub_field('content'); ?>
				</div>
			<?php endif; ?>
			<?php $spread = get_sub_field('spread_logos'); ?>
			<?php if (have_rows('logos')) : ?>
					<div class="col is-12 logos__logo-container <?php echo ($spread) ? 'logos__logo-container--spread' : ''; ?>">
						<?php while (have_rows('logos')) : the_row(); ?>

								<?php $img = get_sub_field('image'); ?>

								<?php if ($style == 'static') : ?>

										<?php // SIMPLE MARKUP ?>
										<?php if (get_sub_field('url')) : ?>
											<a class="load-hidden" href="<?php the_sub_field('url'); ?>">
												<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" title="<?php echo $img['title']; ?>" />
											</a>
										<?php else : ?>
											<img class="load-hidden" src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" title="<?php echo $img['title']; ?>" />						
										<?php endif; ?>
										<?php // END SIMPLE MARKUP ?>

								<?php else : ?>

										<?php // DETAILED MARKUP
										
											$hover_class = '';

											if ((get_sub_field('description')) || (get_sub_field('link_text')) || (get_sub_field('stat'))) : 
												$hover_class = $hover_class.' hover';
											endif;

											if (get_sub_field('stat')) : 
												$hover_class = $hover_class.' js-odometer-hover-trigger';
											endif;
										
										?>

											<div class="detailed-logo-wrapper <?php echo $hover_class; ?> load-hidden">
												<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" title="<?php echo $img['title']; ?>" />																		
												<div class="detailed-logo-wrapper__content">
													<span class="js-odometer-value" data-value="<?php the_sub_field('stat'); ?>"><?php the_sub_field('stat'); ?></span>
													<?php the_sub_field('description'); ?>
													<?php if ((get_sub_field('url')) && (get_sub_field('link_text'))) : ?>
														<a href="<?php the_sub_field('url'); ?>"><?php the_sub_field('link_text'); ?></a>
													<?php endif; ?>
												</div>
											</div>

										<?php // END DETAILED MARKUP ?>

								<?php endif; ?>
								
							<?php endwhile; ?>
					</div>
			<?php endif; ?>
		</div>
	</div>
</section>