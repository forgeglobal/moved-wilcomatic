<?php
	if (have_rows('flexible_blocks')) :
		while (have_rows('flexible_blocks')) : the_row();

			if (get_row_layout() == 'hero') :
				get_template_part('flexible-blocks/blocks/hero');

			elseif (get_row_layout() == 'hero_case_study') :
				get_template_part('flexible-blocks/blocks/hero-case-study');

			elseif (get_row_layout() == 'tiles_two') :
				get_template_part('flexible-blocks/blocks/tiles-two');

			elseif (get_row_layout() == 'tiles_three') :
				get_template_part('flexible-blocks/blocks/tiles-three');
			
			elseif (get_row_layout() == 'logos') :
				get_template_part('flexible-blocks/blocks/logos');
			
			elseif (get_row_layout() == 'content_three_columns') :
				get_template_part('flexible-blocks/blocks/content-three-columns');
			
			elseif (get_row_layout() == 'image_two_columns') :
				get_template_part('flexible-blocks/blocks/image-two-columns');
			
			elseif (get_row_layout() == 'content_and_image') :
				get_template_part('flexible-blocks/blocks/content-and-image');

			elseif (get_row_layout() == 'content_and_statistics') :
				get_template_part('flexible-blocks/blocks/content-and-statistics');

			elseif (get_row_layout() == 'video_and_content') :
				get_template_part('flexible-blocks/blocks/video-and-content');

			endif;

		endwhile;
	endif;

	?>