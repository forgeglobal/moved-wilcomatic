<?php
	$logo = get_field('logo');
	$terms = get_the_terms($post, 'case-study-category');
	$theme = 'blue';

	if ($terms) :
		switch ($terms[0]->slug) :

			case 'automotive' :
				$theme = 'blue-light';
				break;

			case 'rail' : 
				$theme = 'orange';
				break;

			case 'wheel-wash' :
				$theme = 'purple';
				break;

			default : 
					
		endswitch;
	endif;
?>

<a href="<?php the_permalink(); ?>" class="tile theme-<?php echo $theme; ?> <?php echo (has_post_thumbnail()) ? 'darken' : ''; ?>">
	<div class="tile__bg tile__bg--case-study" <?php echo (has_post_thumbnail()) ? 'style="background-image: url('.get_the_post_thumbnail_url().');"' : ''; ?>></div>
	<div class="tile__content load-hidden">
		<?php if (get_field('title')) : ?>
			<h2><?php the_field('title'); ?></h2>
		<?php else : ?>
			<h2><?php the_title(); ?></h2>
		<?php endif; ?>
		<?php if (has_excerpt()) : ?>
			<?php the_excerpt(); ?>
		<?php endif; ?>
		<?php if ($logo) : ?>
			<img class="logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" title="<?php echo $logo['title']; ?>" />
		<?php endif; ?>
		<span class="link-text"><?php echo (get_sub_field('link_text')) ? the_sub_field('link_text') : 'Find out more'; ?></span>
	</div>
</a>