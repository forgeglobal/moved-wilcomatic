<?php $bg_img = get_sub_field('background_image'); ?>
	
<a href="<?php the_sub_field('link_url'); ?>" class="tile theme-<?php the_sub_field('theme'); ?> <?php echo ($bg_img) ? 'darken' : ''; ?>">
	<?php if ($bg_img) : ?>
		<div class="tile__bg" style="background-image: url('<?php echo $bg_img['url']; ?>');"></div>
	<?php endif; ?>
	<div class="tile__content load-hidden">
		<?php if (get_sub_field('title')) : ?>
			<h2><?php the_sub_field('title'); ?></h2>
		<?php endif; ?>
		<?php if (get_sub_field('content')) : ?>
			<?php the_sub_field('content'); ?>
		<?php endif; ?>
		<?php if (get_sub_field('link_text') && get_sub_field('link_url')) : ?>
			<span class="link-text"><?php echo (get_sub_field('link_text')) ? the_sub_field('link_text') : 'Find out more'; ?></span>
		<?php endif; ?>
	</div>
</a>