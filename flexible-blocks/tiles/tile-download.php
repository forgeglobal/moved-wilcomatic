<a class="tile tile--download" href="<?php the_sub_field('link_url'); ?>">
	<div class="tile__bg tile__bg--download theme-<?php the_sub_field('theme'); ?>"></div>
	<div class="tile__content load-hidden">
		<?php if (get_sub_field('title')) : ?>
			<h2><?php the_sub_field('title'); ?></h2>
		<?php endif; ?>
		<?php if (get_sub_field('content')) : ?>
			<?php the_sub_field('content'); ?>
		<?php endif; ?>
		<?php $cover = get_sub_field('download_image'); ?>
		<?php if ($cover) : ?>
			<img class="download-cover" src="<?php echo $cover['url']; ?>" alt="<?php echo $cover['alt']; ?>" title="<?php echo $cover['title']; ?>" />
		<?php endif; ?>
		<span class="link-text"><?php echo (get_sub_field('link_text')) ? the_sub_field('link_text') : 'Find out more'; ?></span>
	</div>
</a>